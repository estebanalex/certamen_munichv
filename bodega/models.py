from django.db import models

class RegistroBodega(models.Model):
    id_registro = models.AutoField(primary_key=True, unique=True)
    nombre_producto = models.CharField(max_length=100, blank=False)
    cantidad_stock = models.IntegerField()
    minimo_stock = models.IntegerField()
    ubicacion_producto = models.CharField(max_length=100, blank=False)
    fecha_ingreso = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.nombre_producto
