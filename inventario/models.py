from django.db import models

class RegistroInventario(models.Model):
    id_registro_inventario = models.AutoField(primary_key=True, unique=True)
    nombre_producto = models.CharField(max_length=100, blank=False)
    cantidad_item = models.IntegerField()
    pertenencia_municipal = models.IntegerField
    pertenencia_departamento = models.IntegerField

    def __str__(self):
        return self.nombre_producto
